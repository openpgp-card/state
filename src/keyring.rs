// SPDX-FileCopyrightText: Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: MIT OR Apache-2.0

use keyring::Entry;

/// "Service" name to identify openpgp-card related entries
const KEYRING_NAME: &str = "openpgp-card";

pub(crate) fn get(ident: &str) -> anyhow::Result<Option<String>> {
    let entry = Entry::new(KEYRING_NAME, ident)?;

    // FIXME: handle None case
    Ok(Some(entry.get_password()?))
}

pub(crate) fn set(ident: &str, pin: &str) -> anyhow::Result<()> {
    let entry = Entry::new(KEYRING_NAME, ident)?;
    Ok(entry.set_password(pin)?)
}

pub(crate) fn delete(ident: &str) -> anyhow::Result<()> {
    let entry = Entry::new(KEYRING_NAME, ident)?;
    Ok(entry.delete_credential()?)
}
