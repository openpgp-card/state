// SPDX-FileCopyrightText: Fina Wilke <fina@felinira.net>
// SPDX-License-Identifier: MIT OR Apache-2.0

use pinentry::PassphraseInput;
use secrecy::ExposeSecret;

pub(crate) fn get(ident: &str) -> anyhow::Result<String> {
    if let Some(mut input) = PassphraseInput::with_default_binary() {
        let passphrase = input
            .with_description(&format!("Please unlock the card\n\nIdent: {}", ident))
            .with_prompt("PIN")
            .interact()
            .map_err(anyhow::Error::msg)?;

        Ok(passphrase.expose_secret().to_string())
    } else {
        Err(anyhow::Error::msg("pinentry binary not found in PATH. Ensure pinentry is installed or select another backend."))
    }
}

pub(crate) fn set(_ident: &str, _pin: &str) -> anyhow::Result<()> {
    // caching the pin is not implemented at the moment
    Ok(())
}

pub(crate) fn delete(_ident: &str) -> anyhow::Result<()> {
    // caching the pin is not implemented at the moment
    Ok(())
}
