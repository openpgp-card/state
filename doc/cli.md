<!--
SPDX-FileCopyrightText: Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: MIT OR Apache-2.0
-->

# CLI tool

After installing this crate (e.g. by running `cargo install --path .`), you can use the `openpgp-card-state` CLI tool to inspect and alter stored state for your cards (including storing and lookup of User PINs in the PIN storage backend).

Ideally, the `openpgp-card-state` CLI tool should not be needed by end users. Applications that leverage the `openpgp-card-state` library should provide appropriate built-in mechanisms to store the User PIN, if possible.

However, for some applications, it may be impractical to handle PIN storage. In such cases, this tool can serve as a fallback.

## Storing a User PIN

To store the User PIN for our example device, we use the "ident" format to address the correct card. One way to look up the `ident` of a card is to [use the `oct` tool](https://codeberg.org/openpgp-card/openpgp-card-tools/#list-cards):

```
$ oct list
Available OpenPGP cards:
0000:01234567
```

In this example, we use a card with the ident `0000:01234567`.

You need to use the identifier of your own card, if you want to play along. Note: this tool doesn't require a card to be plugged in to store metadata about it, it doesn't perform any checks, including verification of the User PIN.

Now that we know our card's ident, we can store its User PIN (the value of the PIN must be entered at the prompt "Enter User PIN"):

```
$ openpgp-card-state put 0000:01234567
Enter User PIN:
stored.
```

### Strong User PINs in the "Direct" backend

To store a User PIN in a non-default backend, you can provide the `--pin-backend` parameter, like this:

```
$ openpgp-card-state put 0000:01234567 --pin-backend direct
Enter User PIN:
stored.
```

The resulting config file section looks like this:

```text
[[cards]]
ident = "0000:01234567"

[cards.pin_storage]
Direct = "123456"
```

## Inspecting a stored User PIN

We can now inspect the stored state for our card:

```
$ openpgp-card-state get 0000:01234567
Persisted User PIN for OpenPGP card 0000:01234567: '123456'
```

If we check in the config file, we'll see that the default `pin_storage` backend was used:

```text
[[cards]]
ident = "0000:01234567"
pin_storage = "Keyring"
```

Now, we can use the card with applications that support `openpgp-card-state`, without needing to perform PIN entry.

## Removing all information about a card

To drop stored information about a card from the backend, we can run:

```
$ openpgp-card-state delete 0000:01234567
deleted.
```

And indeed, when trying to `get` the PIN, the storage backend reports that no matching PIN is found.

```
$ openpgp-card-state get 0000:01234567
Error: Failed to get User PIN for card 0000:01234567: No matching entry found in secure storage
```
