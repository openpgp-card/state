<!--
SPDX-FileCopyrightText: Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: MIT OR Apache-2.0
-->

# Use by application developers

The main purpose of this crate is to be directly integrated into user-facing applications, as a library. One application to look at, for reference, is [openpgp-card-ssh-agent](https://crates.io/crates/openpgp-card-ssh-agent).

Cards are named using the "ident" format, which combines the manufacturer id and serial number of cards (separated by a colon `:` symbol).

## Using a stored User PIN

Applications can obtain a stored User PIN by calling `openpgp_card_state::get_pin(&ident)`. When this call returns an `Ok(Some(pin))`, the application can use `pin` to verify against the card.

If the verification call succeeds, the application can use the card for cryptographic operations.

### Drop on verification error

If the card is available, but the verification call fails, with a stored pin, the application should assume that the stored User PIN is invalid (e.g. because the user has changed the PIN for this card).

In this case, the application should call `openpgp_card_state::drop_pin(&ident)` to drop the User PIN from the storage backend. Dropping the PIN from the backend prevents repeated use of a (presumably wrong) PIN, which may cause the card to lock the User PIN.

## Storing a User PIN

Applications that can obtain a User PIN from the user, with application-appropriate UX can persist it by calling `openpgp_card_state::set_pin(&ident, &pin)`.

Obtaining the User PIN from end-user facing applications and persisting them is the preferred pattern for storing the User PIN with `openpgp_card_state`.

The application should only store a User PIN in `openpgp_card_state` after verifying it against the card, and receiving a positive result.

# Desktop notifications

Notifications are an orthogonal concern, but we think they are an important aspect of building user-friendly applications, especially for applications that aren't directly user-facing.

We suggest that users should be notified of a number of situations that arise around OpenPGP card use:

- missing User PINs, where an application fails to use a card because no User PIN can be found in the PIN storage backend
- failure when attempting to verify a User PIN with a card (this probably means that the stored User PIN is incorrect, and should be dropped from the PIN storage backend, also see above)
- whenever touch confirmation is required to perform a cryptographic operation on a card, the user should be notified of this requirement, including a message that outlines the nature of the operation that will be performed (e.g. which application is performing it, and additional details if possible)